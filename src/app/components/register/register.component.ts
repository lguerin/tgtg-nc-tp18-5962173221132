import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'nc-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;
  email: FormControl;
  name: FormControl;
  password: FormControl;
  confirmPassword: FormControl;
  hasError = false;

  /**
   * Validateur pour les mots de passe
   *
   * @param control Champ FormControl sur lequel est ajouté le validator
   */
  static passwordMatch(control: AbstractControl): ValidationErrors | null {
    const password = control?.parent?.get('password').value ?? '';
    const confirmPassword = control?.parent?.get('confirmPassword').value ?? '';
    return password !== confirmPassword ? { passwordMatchError: true } : null;
  }

  constructor(private fb: FormBuilder,
              private authenticationService: AuthenticationService,
              private router: Router) { }

  ngOnInit(): void {
    this.initUserForm();
  }

  /**
   * Initialiser le formulaire de création de compte utilisateur
   */
  initUserForm(): void {
    // Init des champs
    this.email = this.fb.control('', [Validators.required, Validators.email]);
    this.name = this.fb.control('', [Validators.required]);
    this.password = this.fb.control('', [Validators.required, Validators.minLength(5)]);
    this.confirmPassword = this.fb.control('', [Validators.required,
      Validators.minLength(5), RegisterComponent.passwordMatch]);

    // Build du formulaire
    this.userForm = this.fb.group({
      email: this.email,
      name: this.name,
      password: this.password,
      confirmPassword: this.confirmPassword
    });
  }

  getErrorMessage(control: FormControl, minLength: number = 0): string {
    let error = '';
    if (control.hasError('required')) {
      error = 'Champ obligatoire';
    }
    if (control.hasError('email')) {
      error = 'Email invalide';
    }
    if (control.hasError('minlength')) {
      error = `Minimum ${minLength} caractères`;
    }
    if (control.hasError('passwordMatchError')) {
      error = 'Les mots de passes diffèrent';
    }
    return error;
  }

  /**
   * Créer un nouvel utilisateur
   */
  createUser(): void {
    this.hasError = false;
    const form = this.userForm.value;
    this.authenticationService.register(form.email, form.name, form.password).subscribe(
      () => {
        this.authenticationService.authenticate(form.email, form.password).subscribe(
          () => this.router.navigate(['/']),
          () => this.hasError = true
        );
    },
      () => this.hasError = true
    );
  }
}
