import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreModel } from '../../../models/store.model';
import { ReferentielService } from '../../../services/referentiel.service';
import { Roles } from '../../../constants/roles.constant';

@Component({
  selector: 'nc-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  store: StoreModel;
  categories: Array<string> = [];

  readonly Roles = Roles;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private referentielService: ReferentielService) { }

  ngOnInit(): void {
    this.store = this.route.snapshot.data.store;
    this.referentielService.getStoreCategories().subscribe(list => this.categories = list);
  }

  back(): void {
    this.router.navigate(['/']);
  }
}
