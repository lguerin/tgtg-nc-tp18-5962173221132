/**
 * Rôles applicatifs
 */
export class Roles {
  static USER = 'ROLE_USER';
  static ADMIN = 'ROLE_ADMIN';
}
